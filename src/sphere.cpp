#define NDEBUG
#include "sphere.h"
#include <iostream>
#include <math.h>

void sphere(
  const int num_faces_u,
  const int num_faces_v,
  Eigen::MatrixXd & V,
  Eigen::MatrixXi & F,
  Eigen::MatrixXd & UV,
  Eigen::MatrixXi & UF,
  Eigen::MatrixXd & NV,
  Eigen::MatrixXi & NF)
{
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here:
  if (num_faces_u <= 0 || num_faces_v <= 0){
    return;
    
  }
  V.resize((num_faces_u +1) * (num_faces_v+1),3);
  UV.resize((num_faces_u+1) * (num_faces_v+1),2);
  NV.resize((num_faces_u+1) * (num_faces_v+1),3);
  F.resize((num_faces_u) * (num_faces_v),4);
  UF.resize((num_faces_u) * (num_faces_v),4);
  NF.resize((num_faces_u) * (num_faces_v),4);

  double x, y, z;
      
      
  const double long_incr = 1.0/num_faces_u;
  const double lat_incr = 1.0/num_faces_v;
  
  // loop through all the points in the texture coordinates

  //getting x,y,z from uv: https://stackoverflow.com/questions/7840429/calculate-the-xyz-point-of-a-sphere-given-a-uv-coordinate-of-its-texture
  
  for (int i=0; i<=num_faces_v; i++) {
      for (int j=0; j<=num_faces_u; j++) {
          int index = i * (num_faces_u +1) + j;
          const double u = j*long_incr;
          const double v = i*lat_incr;
          const double _v = v*M_PI-M_PI/2.0;
          const double _u = u*2.0*M_PI-M_PI;
          x = cos(_v)*cos(_u);
          y = cos(_v)*sin(_u);
          z = sin(_v);
          //because the sphere is at the origin, points on its surface also happen to be surface normal vectors
          V.row(index) = Eigen::RowVector3d(x, y, z);
          UV.row(index) = Eigen::RowVector2d(u, v);
          NV.row(index) = Eigen::RowVector3d(x, y, z);
      }
  }

    //face indexes
    for (int i=0; i<num_faces_v; i++) {
        for (int j=0; j<num_faces_u; j++) {
            int index = i * (num_faces_u) + j;
            //bottom left, top left, top right, then bottom left indexes on the UV map, which correspond to the created vertices
            Eigen::RowVector4i face(index + i, index + i + 1, index + i + num_faces_u + 2, index + i + num_faces_u + 1); 
            F.row(index) = face;
            UF.row(index) = face;
            NF.row(index) = face;
        }
    }

  
  
  ////////////////////////////////////////////////////////////////////////////
}
