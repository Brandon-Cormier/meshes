#include "triangle_area_normal.h"
#include <Eigen/Geometry>

Eigen::RowVector3d triangle_area_normal(
  const Eigen::RowVector3d & a, 
  const Eigen::RowVector3d & b, 
  const Eigen::RowVector3d & c)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code:
  ////////////////////////////////////////////////////////////////////////////
  //area of a triangle from 
  //http://www.maths.usyd.edu.au/u/MOW/vectors/vectors-11/v-11-7.html
  Eigen::RowVector3d AC = a-c;
  Eigen::RowVector3d BC = b-c;
  Eigen::RowVector3d normal = AC.cross(BC).normalized();
  double area = AC.cross(BC).norm()/2; 
  return area * normal;
}
