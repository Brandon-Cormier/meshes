#include "per_corner_normals.h"
#include "triangle_area_normal.h"
// Hint:
#include "vertex_triangle_adjacency.h"
#include <iostream>

void per_corner_normals(
  const Eigen::MatrixXd & V,
  const Eigen::MatrixXi & F,
  const double corner_threshold,
  Eigen::MatrixXd & N)
{
  N = Eigen::MatrixXd::Zero(F.rows()*3,3);
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here:
  //build adjacency faces for the vertices
  std::vector<std::vector<int> > VF;
  vertex_triangle_adjacency(F, V.rows(), VF);
  int idx = 0;
  //loop through every vertex on every face
  for (int i = 0; i < F.rows(); i ++){
    Eigen::RowVector3d this_normal = triangle_area_normal(V.row(F(i,0)), V.row(F(i,1)), V.row(F(i,2))); //face normal for that vertex
    for (int j = 0; j < F.cols(); j++){
      int index = F(i, j); //vertex index
      Eigen::RowVector3d total = this_normal; //total for wighted average
      double area = this_normal.norm(); //area to average out the nomral later
      for (int k = 0; k < VF[index].size(); k++){ //loop for every face adjacent to the index
        int other_face = VF[index][k];
        Eigen::RowVector3d other_normal = triangle_area_normal(V.row(F(other_face,0)), V.row(F(other_face,1)), V.row(F(other_face,2)));
        double angle = acos(this_normal.normalized().dot(other_normal.normalized())) * 180 / M_PI; //corner angle
        if( angle < corner_threshold && other_face != i){ //threshold check
          total += other_normal;
          area += other_normal.norm();
        }
      }

      N.row(idx) = (total/area).normalized(); //set the normal to the average
      idx++;
    }
  }

  ////////////////////////////////////////////////////////////////////////////
}
