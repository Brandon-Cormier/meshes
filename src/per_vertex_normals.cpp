#include "per_vertex_normals.h"
#include "triangle_area_normal.h"
#include <map>
#include <vector>

void per_vertex_normals(
  const Eigen::MatrixXd & V,
  const Eigen::MatrixXi & F,
  Eigen::MatrixXd & N)
{
  N = Eigen::MatrixXd::Zero(V.rows(),3);
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here:
  // faces that a point belongs to
  std::map<int, std::vector<int>> point_faces;
  for (int i=0; i<F.rows(); i++) {
      for (int j=0; j<F.cols(); j++) {
          point_faces[F(i, j)].push_back(i);
      }
  }

  //average out the faces
  for(int i = 0; i < V.rows(); i ++){
    std::vector<int> faces = point_faces[i];
    Eigen::RowVector3d total(0,0,0);
    for(int j = 0; j < faces.size(); j++){
      Eigen::RowVector3d a = V.row(F(faces[j], 0));
      Eigen::RowVector3d b = V.row(F(faces[j], 1));
      Eigen::RowVector3d c = V.row(F(faces[j], 2));
      total += triangle_area_normal(a,b,c);
    }
    N.row(i) = total/faces.size();
  }
  
  ////////////////////////////////////////////////////////////////////////////
}
