# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/brandon/Documents/CSC418/a5/computer-graphics-meshes/obj.cpp" "/home/brandon/Documents/CSC418/a5/computer-graphics-meshes/build/CMakeFiles/obj.dir/obj.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../libigl/cmake/../include"
  "../libigl/cmake/../external/eigen"
  "../libigl/external/glad/include"
  "../libigl/external/glfw/include"
  "../libigl/external/stb"
  "stb_image"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/brandon/Documents/CSC418/a5/computer-graphics-meshes/build/CMakeFiles/core.dir/DependInfo.cmake"
  "/home/brandon/Documents/CSC418/a5/computer-graphics-meshes/build/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/brandon/Documents/CSC418/a5/computer-graphics-meshes/build/stb_image/CMakeFiles/igl_stb_image.dir/DependInfo.cmake"
  "/home/brandon/Documents/CSC418/a5/computer-graphics-meshes/build/glad/CMakeFiles/glad.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
